from setuptools import setup, find_packages
setup(
    name="pymaphys",
    version="0.1",
    description="a wrapper to call the MaPHyS solver",
    url="https://gitlab.inria.fr/gmarait/PYMAPHYS",
    author="Gilles Marait",
    author_email="gilles.marait@inria.fr",
    license="CeCILL-C",
    packages=find_packages(),
)
