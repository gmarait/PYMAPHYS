"""
PyMaPHyS
"""
from ctypes import *
from ctypes.util import find_library

__authors__ = "Gilles Marait"
__versions__ = "0.1"

from .Maphys import *

__all__ = ['Maphys']
