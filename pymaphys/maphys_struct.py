#
# Python wrapper for maphys
# 24/03/17
# Tested on mumps version 0.9.5.0
# Author: Gilles Marait
#

from ctypes import *
from mpi4py import MPI

class Maphys_struct():

    ###
    # Maphys scalar data types

    class maphys_complex(Structure):
        _fields_ = [("r", c_float),
                    ("i", c_float)]

    class maphys_double_complex(Structure):
        _fields_ = [("r", c_double),
                    ("i", c_double)]

    # Create the maphys structures

    class cmaphys_struct(Structure):
        pass

    class dmaphys_struct(Structure):
        pass

    class smaphys_struct(Structure):
        pass

    class zmaphys_struct(Structure):
        pass

    #
    ### For version 0.9.6

    MAPHYS_STRL         = 1024
    MAPHYS_ICNTL_SIZE   = 57
    MAPHYS_RCNTL_SIZE   = 21
    MAPHYS_IINFO_SIZE   = 37
    MAPHYS_RINFO_SIZE   = 38
    MAPHYS_IINFOG_SIZE  =  6
    MAPHYS_RINFOG_SIZE  =  4
    MAPHYS_IKEEP_SIZE   = 66
    MAPHYS_RKEEP_SIZE   = 21

    def __init__(self, precision):
        precs = ['c', 'd', 's', 'z']
        if precision not in precs:
            raise ValueError("Precision given is " + str(precision) + ". Not in " + str(precs))

        self.precision = precision

    def get_struct_type(self):
        try:
            Maphys_struct.cmaphys_struct._fields_ = self.__get_fields('c')
            Maphys_struct.dmaphys_struct._fields_ = self.__get_fields('d')
            Maphys_struct.smaphys_struct._fields_ = self.__get_fields('s')
            Maphys_struct.zmaphys_struct._fields_ = self.__get_fields('z')
        except AttributeError:
            # Ignore error if the values are already set
            pass
        struct_dict = {'c' : Maphys_struct.cmaphys_struct,
                       'd' : Maphys_struct.dmaphys_struct,
                       's' : Maphys_struct.smaphys_struct,
                       'z' : Maphys_struct.zmaphys_struct}
        return struct_dict[self.precision]

    def get_scal_type(self):
        scal_dict = {'c' : Maphys_struct.maphys_complex,
                     'd' : c_double,
                     's' : c_float,
                     'z' : Maphys_struct.maphys_double_complex}
        return scal_dict[self.precision]

    def get_real_type(self):
        real_dict = {'c' : c_float,
                     'd' : c_double,
                     's' : c_float,
                     'z' : c_double}
        return real_dict[self.precision]

    def get_numpy_type(self):
        np_dict = {'c' : 'c8',
                   'd' : 'f8',
                   's' : 'f4',
                   'z' : 'c16'}

        return np_dict[self.precision]

    def get_MPI_comm_type(self):
        if MPI._sizeof(MPI.Comm) == sizeof(c_long):
            return c_long
        elif MPI._sizeof(MPI.Comm) == sizeof(c_int):
            return c_int
        else:
            return c_void_p

    def __get_fields(self, precision):
        """
        Create the structure fields depending on the scalar type
        """
        scal = self.get_scal_type()
        MPI_Comm = self.get_MPI_comm_type()

        ICNTL_SIZE = self.MAPHYS_ICNTL_SIZE
        RCNTL_SIZE = self.MAPHYS_RCNTL_SIZE
        IINFO_SIZE = self.MAPHYS_IINFO_SIZE
        RINFO_SIZE = self.MAPHYS_RINFO_SIZE
        IINFOG_SIZE = self.MAPHYS_IINFOG_SIZE
        RINFOG_SIZE = self.MAPHYS_RINFOG_SIZE

        STR_SIZE = self.MAPHYS_STRL

        return  [("comm", MPI_Comm),
                 ("fcomm", c_int),
                 ("sym", c_int),
                 ("n", c_int),
                 ("nnz", c_int),
                 ("rows", POINTER(c_int)),
                 ("cols", POINTER(c_int)),
                 ("values", POINTER(scal)),
                 ("permtab", POINTER(c_int)),
                 ("sizetab", POINTER(c_int)),
                 ("nrhs", c_int),
                 ("rhs", POINTER(scal)),
                 ("sol", POINTER(scal)),
                 ("job", c_int),
                 ("icntl", c_int * ICNTL_SIZE),
                 ("rcntl", c_double * RCNTL_SIZE),
                 ("version", c_char * (STR_SIZE+2)),
                 ("iinfo", c_int * IINFO_SIZE),
                 ("rinfo", c_double * RINFO_SIZE),
                 ("iinfomin", c_int * IINFO_SIZE),
                 ("iinfomax", c_int * IINFO_SIZE),
                 ("iinfoavg", c_double * IINFO_SIZE),
                 ("iinfosig", c_double * IINFO_SIZE),
                 ("rinfomin", c_double * RINFO_SIZE),
                 ("rinfomax", c_double * RINFO_SIZE),
                 ("rinfoavg", c_double * RINFO_SIZE),
                 ("rinfosig", c_double * RINFO_SIZE),
                 ("iinfog", c_int * IINFOG_SIZE),
                 ("rinfog", c_double * RINFOG_SIZE)]
