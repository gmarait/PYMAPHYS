# -*- coding:utf-8 -*-

import scipy.sparse as spp
import numpy as np
import random as rd
import yaml
import time
import re
#import pypastix as pp

################################################################################
# Available functions :
# - get_help
# - yaml_importer
# - strip_cw
# - sparse_mat_import
# - maphys__set_control
# - mumps__set_control
# - generate_rhs
# - get_output
# - drive
################################################################################

def get_help():

    print("This script is used to bench two parallel equation system solvers : Mumps")
    print("and MaPHyS. The script is to be used as follows :")
    print('\033[1;31m')
    print("python bench.py [options] <input file>")
    print('\033[0m')
    print("Available options for this command are:")
    print("   - '-h' and '--help' : ")
    print("   - '-s' and '--simple-precision' : ")
    print("   - '-r' and '--random-rhs : ")

def yaml_importer(filepath):
    with open(filepath, 'r') as f:
        return yaml.load(f.read())

def strip_cw(t):
    i = 0
    while i < len(t):
        if t[i] == '':
            t.remove(t[i])
            i -= 1
        elif t[i][0] == u'#':
            t.remove(t[i])
            i -= 1
        i += 1


def pastix__init(A):
    spmA = pp.spm(A)

    # Initialize parameters to default values
    iparm, dparm = pp.initParam()

    # Startup PaStiX
    pastix_data = pp.init( iparm, dparm )

    return spmA, iparm, dparm, pastix_data

def pastix__fin(pastix_data, iparm, dparm):
    pp.finalize( pastix_data, iparm, dparm )

def maphys__set_control(values, dm):
    if 'ICNTL' in values:
        for i in values['ICNTL']:
            dm.ICNTL[int(i)] = int(values['ICNTL'][i])
    if 'RCNTL' in values:
        for i in values['RCNTL']:
            dm.RCNTL[int(i)] = int(values['RCNTL'][i])

    dm.ICNTL[4] = 2
    dm.ICNTL[5] = 0
    dm.ICNTL[6] = 0

def mumps__set_control(values, dm):
    if 'ICNTL' in values:
        for i in values['ICNTL']:
            dm.ICNTL[int(i)] = int(values['ICNTL'][i])
    if 'CNTL' in values:
        for i in values['CNTL']:
            dm.CNTL[int(i)] = int(values['CNTL'][i])

    #dm.ICNTL[1]=0
    #dm.ICNTL[2]=6
    #dm.ICNTL[3]=0
    #dm.ICNTL[4]=1

def sparse_mat_import(filepath, is_precision_double):
    """
    Automatic importation of sparse matrix from an external file. Supported
    formats are Matrix Market .mtx format and Coordinate .ijv format.
    """

    is_complex=False

    def get_datatype(is_complex, is_precision_double):
        if is_precision_double:
            if is_complex:
                return np.complex128
            else:
                return np.float64
        else:
            if is_complex:
                return np.complex64
            else:
                return np.float32

    float_str = "[+-]?\d*\.?\d+(?:[eE][-+]?\d+)?"

    ########################################
    # Matrix file
    ########################################

    if filepath[-4:]=='.mtx' or filepath[-3:]=='.mm':

        with open(filepath, "r") as fin:
            l = fin.readline().decode('utf-8') # Header
            if 'real' in l:
                is_complex = False
                ijv_str = "(\d+)\s+(\d+)\s+(" + float_str + ")"
            elif 'complex' in l:
                is_complex = True
                ijv_str = "(\d+)\s+(\d+)\s+(" + float_str + ")\s+(" + float_str + ")"
            else:
                raise ValueError("Matrix file not recognized")

            ijv_regex = re.compile(ijv_str)

            nnz = 0
            while nnz == 0 and l:
                l = fin.readline().decode('utf-8')
                vals = l.split()
                if vals[0][0] == '%':
                    continue
                m = int(vals[0])
                n = int(vals[1])
                nnz = int(vals[2])

            datatype = get_datatype(is_complex, is_precision_double)

            irn = np.zeros((nnz,), dtype='i')
            jcn = np.zeros((nnz,), dtype='i')
            data = np.zeros((nnz,), dtype=datatype)

            idx = 0
            while l:
                vals = re.search(ijv_regex, l)
                if vals:
                    if is_complex:
                        val = complex(vals.group(3)) + 1.j * float(vals.group(4))
                    else:
                        val = float(vals.group(3))
                    if val == 0:
                        l = fin.readline().decode('utf-8')
                        continue
                    irn[idx] = int(vals.group(1)) - 1
                    jcn[idx] = int(vals.group(2)) - 1
                    data[idx] = val
                    idx += 1
                l = fin.readline().decode('utf-8')

    ########################################
    # Vector file
    ########################################

    elif name[-4:]=='.ijv':

        with open(filepath, "r") as fin:
            l = fin.readline().decode('utf-8') # Header
            vals = l.split()
            m = int(vals[0])
            n = int(vals[1])
            nnz = int(vals[2])

            l = fin.readline().decode('utf-8') # First line
            if len(l.split()) == 2:
                is_complex = True
                ijv_str = "(\d+)\s+(\d+)\s+(" + float_str + ")"
            else:
                is_complex = False
                ijv_str = "(\d+)\s+(\d+)\s+(" + float_str + ")\s+(" + float_str + ")"

            datatype = get_datatype(is_complex, is_precision_double)

            irn = np.zeros((nnz,), dtype='i')
            jcn = np.zeros((nnz,), dtype='i')
            data = np.zeros((nnz,), dtype=datatype)

            idx = 0
            while l:
                vals = re.search(ijv_regex, l)
                if vals:
                    irn[idx] = int(vals.group(1)) - 1
                    jcn[idx] = int(vals.group(2)) - 1
                    if is_complex:
                        data[idx] = float(vals.group(3)) + 1.j * float(vals.group(4))
                    else:
                        data[idx] = float(vals.group(3))
                    idx += 1
                l = fin.readline().decode('utf-8')

    Asparse = spp.coo_matrix((data, (irn,jcn)), shape=(m,n))
    return Asparse, m, n, is_complex

def generate_rhs(matrix, length, width, NRHS, random_rhs, is_complex, is_precision_double):
    """
    Automatic generation of right-hand side if it isn't already supplied by the
    user in the config file.
    """

    if is_complex:
        scaltype= np.complex128 if is_precision_double else np.complex64
    else:
        scaltype= np.float64 if is_precision_double else np.float32

    if NRHS == 1:
        sol_shape = (width,)
    else:
        sol_shape = (width, NRHS)

    if random_rhs:

        th_sol = np.empty((sol_shape), dtype=scaltype)
        if is_complex:
            sol_gen = np.random.ranf(sol_shape) + np.random.ranf(sol_shape)*1.0j
        else:
            sol_gen = np.random.ranf(sol_shape)
        th_sol[:] = sol_gen[:]

    else:

        th_sol = np.arange(1, width*NRHS+1, dtype=scaltype).reshape(sol_shape)
        if is_complex:
            th_sol[:] += 1.0j

    #rhs = np.matmul(matrix.todense(), th_sol)
    rhs = matrix * th_sol

    return rhs, th_sol

def maphys__drive(driver):
    start = time.time()
    driver.drive(1)
    analysis = time.time()
    driver.drive(2)
    facto = time.time()
    driver.drive(3)
    precond = time.time()
    driver.drive(4)
    end = time.time()
    return [start, analysis, facto, precond, end]

def mumps__drive(driver):
    start = time.time()
    driver.drive(1)
    analysis = time.time()
    driver.drive(2)
    facto = time.time()
    driver.drive(3)
    end = time.time()
    return [start, analysis, facto, end]

def pastix__drive(pastix_data, spmA, rhs):
    t0 = time.time()
    # Perform analyze
    pp.task_analyze(pastix_data, spmA)

    t1 = time.time()

    # Perform numerical factorization
    pp.task_numfact(pastix_data, spmA)

    t2 = time.time()

    # Perform solve
    x = rhs.copy()
    pp.task_solve(pastix_data, x)

    t3 = time.time()

    # Refine the solution
    pp.task_refine(pastix_data, rhs, x)

    t4 = time.time()

    return [t0,t1,t2,t3,t4]

def maphys__benching(driver, time, i, csv):
    print("\n=== Résultats du benching - Run n°{} ===\n".format(i))
    print("Pour MaPHyS :")
    print("\tTemps utilisé : ")
    print("\t\t- Analyse : {} s".format(time[1] - time[0]))
    print("\t\t- Factorisation : {} s".format(time[2] - time[1]))
    print("\t\t- Préconditionnement : {} s".format(time[3] - time[2]))
    print("\t\t- Résolution : {} s".format(time[4] - time[3]))
    print("\t\t- Temps total : {} s".format(time[4] - time[0]))
    print("\tPic de mémoire utilisée : {} Mo".format(driver.IINFO[35]))
    with open(csv, 'a') as f:
        f.write('{},Maphys,{},{},{},{},{}\n'.format(i,time[1] - time[0],time[2] - time[1],time[4] - time[3],time[3] - time[2],driver.IINFO[35]))

def mumps__benching(driver, time, i, csv):
    print("\n=== Résultats du benching - Run n°{} ===\n".format(i))
    print("Pour MUMPS :")
    print("\tTemps utilisé :")
    print("\t\t- Analyse : {} s".format(time[1] - time[0]))
    print("\t\t- Factorisation : {} s".format(time[2] - time[1]))
    print("\t\t- Résolution : {} s".format(time[3] - time[2]))
    print("\t\t- Temps total : {} s".format(time[3] - time[0]))
    print("\tPic de mémoire utilisée : {} Mo".format(max(driver.INFOG[22], driver.INFOG[31])))
    with open(csv, 'a') as f:
        f.write('{},Mumps,{},{},{},,{}\n'.format(i,time[1] - time[0],time[2] - time[1],time[3] - time[2],max(driver.INFOG[22], driver.INFOG[31])))

def pastix__benching(time):
    print("\n=== Résultats du benching - Run n°{} ===\n".format(i))
    print("Pour Pastix :")
    print("\tTemps utilisé :")
    print("\t\t- Analyse : {} s".format(time[1] - time[0]))
    print("\t\t- Factorisation : {} s".format(time[2] - time[1]))
    print("\t\t- Résolution : {} s".format(time[3] - time[2]))
    print("\t\t- Raffinement : {} s".format(time[4] - time[3]))
    print("\t\t- Temps total : {} s".format(time[4] - time[0]))
    with open(csv, 'a') as f:
        f.write('{},Pastix,{},{},{},,\n'.format(i,time[1] - time[0],time[2] - time[1], time[3] - time[2]))
