# -*- coding:utf-8 -*-

from mpi4py import MPI
from pymaphys import Maphys
from pymumps import Mumps
from sys import argv
from yaml import load, dump, Loader, Dumper
from preproc import *
#import pypastix as pp
#import scipy.sparse as spp
import numpy as np
import time

# MPI Stuff
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
nprocs = comm.Get_size()

if len(argv) == 2 and (argv[1] == '-h' or argv[1] == '--help'):
    if rank == 0:
        get_help()

elif len(argv) >= 2:

    # Precision setup
    if '-s' in argv or '--simple-precision' in argv and rank == 0:
        is_precision_double = False
    else:
        is_precision_double = True

    # RHS generation setup
    if '-r' in argv or '--random-rhs' in argv and rank == 0:
        is_rhs_random = True
    else:
        is_rhs_random = False

    #Splicing of the config file
    yl = yaml_importer(argv[-1])

    is_complex=False
    if rank == 0:
        print("Importing matrix")
        A, width, length, is_complex = sparse_mat_import(yl['General']['MATFILE'], is_precision_double)

    is_complex = comm.bcast(is_complex, root = 0)

    if rank == 0 and "RHSFILE" in yl["General"]:
        rhs, __, __, __ = sparse_mat_import(yl['General']['RHSFILE'], is_precision_double)
        rhs = rhs.todense()
        rhs_found = True
    elif rank == 0:
        rhs, th_sol = generate_rhs(A, length, width, 1, is_rhs_random, is_complex, is_precision_double)
        rhs_found = False

    # Initialize maphys and mumps wrappers
    if is_complex :
        if is_precision_double:
            driver_maphys = Maphys('z')
            driver_mumps = Mumps('z')
        else:
            driver_maphys = Maphys('c')
            driver_mumps = Mumps('c')
    else :
        if is_precision_double:
            driver_maphys = Maphys('d')
            driver_mumps = Mumps('d')
        else:
            driver_maphys = Maphys('s')
            driver_mumps = Mumps('s')

    driver_mumps.sym = int(yl["General"]["SYM"])
    driver_maphys.sym = int(yl["General"]["SYM"])

    csv_filename = yl["General"]["CSV_OUTPUT"]
    with open(csv_filename, 'w') as f:
        f.write('No. run,Solveur,Analyse,Factorisation,Résolution,Préconditionnement,Mémoire\n')

    # Set fortran communicator
    ### MaPHyS
    mphs = driver_maphys.mphs
    driver_maphys.set_comm(comm)

    ### Mumps
    id = driver_mumps.id
    id.comm_fortran = comm.py2f()
    id.par = 1

    for i in range(1,len(yl)):

        if rank == 0:
            print("Run n°{}".format(i))

        # Init maphys

        if 'Pastix' in yl["Run " + str(i)]:
            pa_A, ip, dp, pa_data = pastix__init(A)
            pa_time = pastix__drive(pa_data, pa_A, rhs)
            pastix__fin(pa_data, ip, dp)

        if 'Maphys' in yl["Run " + str(i)]:
            driver_maphys.initialize()

            if rank == 0:
                driver_maphys.set_A(A)
                driver_maphys.set_RHS(rhs)

            maphys__set_control(yl["Run "+str(i)]['Maphys'], driver_maphys)
            ma_time = maphys__drive(driver_maphys)

        if 'Mumps' in yl["Run " + str(i)]:
            driver_mumps.initialize()

            if rank == 0:
                driver_mumps.set_A(A)
                driver_mumps.set_RHS(rhs)

            mumps__set_control(yl["Run "+str(i)]['Mumps'], driver_mumps)
            mu_time = mumps__drive(driver_mumps)

        if rank == 0:
            if 'Maphys' in yl["Run " + str(i)]:
                maphys__benching(driver_maphys, ma_time, i, csv_filename)
            if 'Mumps' in yl["Run " + str(i)]:
                mumps__benching(driver_mumps, mu_time, i, csv_filename)
            if 'Pastix' in yl['Run ' + str(i)]:
                pastix__benching(time)

            sol_maphys = driver_maphys.get_solution()
            sol_mumps = driver_mumps.get_solution()

        if 'Maphys' in yl["Run " + str(i)]:
            driver_maphys.finalize()
        if 'Mumps' in yl["Run " + str(i)]:
            driver_mumps.finalize()

            """
            print("Found:")
            print(sol_maphys[:min(20, len(sol_maphys))])
            print(sol_mumps[:min(20, len(sol_mumps))])
            if not(rhs_found):
                print("Th. solution:")
                print(th_sol[:min(20,len(th_sol))])
            print("Backward error:")
            print(be)
            """
        # Finalize solvers

    comm.Barrier()
    if rank == 0:
        print("done")

else:
    raise RuntimeError("Command does not know how to work. Please run with option '-h' or '--help'.")
