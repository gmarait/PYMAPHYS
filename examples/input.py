# Python template

def get_genfem_param():
    cube_base = 3
    heterogeneity = 10000
    nLineK = 3
    return cube_base, heterogeneity, nLineK

def get_input(m):

    m.mphs.sym = 1

    # Verbose
    m.ICNTL[4] = 5
    m.ICNTL[5] = 1
    m.ICNTL[6] = 1
    m.ICNTL[7] = 4

    # Basic MaPHyS parameters
    m.ICNTL[13] = 1
    m.ICNTL[20] = 2
    m.ICNTL[21] = 1
    m.ICNTL[24] = 500
    m.ICNTL[26] = 500
    m.RCNTL[11] = 1e-4
    m.RCNTL[21] = 1e-6

    # CGC options
    m.ICNTL[33] = 5
    m.ICNTL[51] = 2
    m.ICNTL[52] = 1

    # Mumps memory errors
    m.ICNTL[47] = 200
    m.ICNTL[50] = -1
    m.RCNTL[5]  = 2

    # Multithreading options
    m.ICNTL[42] = 0
    m.ICNTL[37] = 1
    m.ICNTL[38] = 24
    m.ICNTL[39] = 1
    m.ICNTL[40] = 4
