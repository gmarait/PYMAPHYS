from mpi4py import MPI
from pymaphys import Maphys
import scipy.sparse as spp
import numpy as np

# Initialize maphys wrapper
driver_maphys = Maphys('d')
# Get corresponding numpy type
nptype = driver_maphys.nptype

# MPI stuff
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
nprocs = comm.Get_size()

# Set fortran communicator
mphs = driver_maphys.mphs
driver_maphys.set_comm(comm)

# Init maphys
driver_maphys.initialize()

n = 9
A = np.zeros((n,n))
# Blocks for A
BDiag = np.zeros((3,3))

for i in range(3):
    BDiag[i,i] = 4.0
for i in range(2):
    BDiag[i,i+1]=-1.0
    BDiag[i+1,i]=-1.0

BExt = np.zeros((3,3))

for i in range(3):
    BExt[i,i] = -1.0

# Create A
for i in range(0, int(n/3)):
    A[i*3:(i+1)*3 , i*3:(i+1)*3] = BDiag

for i in range(0, int(n/3 - 1)):
        A[i*3:(i+1)*3 , (i+1)*3:(i+2)*3] = BExt
        A[(i+1)*3:(i+2)*3 , i*3:(i+1)*3] = BExt

# Share the interface values between the two domains
A[6:9,6:9] = A[6:9,6:9]/2.0

n_glob = 15
NRHS = 2
mphs.sym = 0

th_sol = np.zeros((n, NRHS,), dtype=nptype)

# Set rhs
rhs = np.zeros((n, NRHS), dtype=nptype)
if rank == 0:
    rhs[:,0] = [-2.0,-1.0,4.0,3.0,0.0,7.0,6.0,0.0,10.0]
    rhs[:,1] = [28.0,14.0,34.0,18.0,0.0,22.0,21.0,0.0,25.0]
    reorder = []
    th_sol[:,0] = np.arange(1.0, 10.0, dtype=nptype)
    th_sol[:,1] = np.arange(16.0, 25.0, dtype=nptype)
else:
    rhs[:,0] = [28.0,17.0,34.0,9.0,0.0,13.0,0.0,0.0,0.0]
    rhs[:,1] = [58.0,32.0,64.0,24.0,0.0,28.0,0.0,0.0,0.0]
    reorder = [6,7,8,3,4,5,0,1,2]
    th_sol[reorder[:],0] = np.arange(7, 16, dtype=nptype)
    th_sol[reorder[:],1] = np.arange(22, 31, dtype=nptype)

driver_maphys.set_A(A)
driver_maphys.set_RHS(rhs)

# Set icntl (careful: Fortran numbering)
driver_maphys.ICNTL[4] = 5
driver_maphys.ICNTL[5] = 1
driver_maphys.ICNTL[6] = 1
driver_maphys.ICNTL[7] = 4
driver_maphys.ICNTL[20] = 3
driver_maphys.ICNTL[21] = 1
driver_maphys.ICNTL[24] = 500
driver_maphys.ICNTL[26] = 500
driver_maphys.ICNTL[28] = 0

# Distribution (python numbering)
myinterface = [6,7,8]
myindexvi = [1-rank]
myptrindexvi = [0,3]
myindexintrf = [0,1,2]

driver_maphys.distributed_interface(myinterface, myindexvi, myptrindexvi, myindexintrf)

# Call maphys
driver_maphys.drive(6)

be = driver_maphys.RINFOG[2]

if rank == 0:
    print("Backward error:")
    print(be)

for p in range(2):
    if rank == p:
        sol = driver_maphys.get_solution()
        print("For proc: " + str(rank))
        if reorder:
            print("Th. solution:")
            print(th_sol[reorder])
            print("Found:")
            print(sol[reorder])
        else:
            print("Th. solution:")
            print(th_sol)
            print("Found:")
            print(sol)

    comm.Barrier()

# Finalize maphys
driver_maphys.finalize()

if rank == 0:
    print("done")
