import re

re_cntl = re.compile('(I|R)CNTL\((\d+)\)\s+=\s+(.+)')

def parse(filename):
    with open(filename, 'r') as f:
        ICNTLS = {}
        RCNTLS = {}
        OTHER = {}
        for l in f:
            if '#' in l:
                linetreat = l.split('#')[0]
            else:
                linetreat = l

            # Match i/r cntl
            match = re.search(re_cntl, linetreat)
            if match:
                id = int(match.groups()[1])
                if match.groups()[0] == 'I':
                    val = int(match.groups()[2])
                    ICNTLS[id] = val
                else:
                    val = float(match.groups()[2])
                    RCNTLS[id] = val
            # Match other keys
            elif '=' in linetreat:
                key = linetreat.split('=')[0].strip()
                val = linetreat.split('=')[1].strip()
                OTHER[key] = val
                
    return ICNTLS, RCNTLS, OTHER
