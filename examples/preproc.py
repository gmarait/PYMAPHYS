# -*- coding:utf-8 -*-

import scipy.sparse as spp
import numpy as np
import random as rd
import sys

###############################################################################
# Available functions :
# - parser
# - strip_cw
# - sparse_mat_import
# - generate_rhs
###############################################################################

def get_help():
    """
    Prints basic help.
    """
    print("This script is a test file for MaPHyS, a hybrid matrix system solver developed")
    print("at INRIA Bordeaux. The program is to be used as follows :")
    print('\033[1;31m')
    print("python exemple.py <options> file")
    print('\033[0m')
    print("Available options for this command are :")
    print("   - '-h' and '--help' : Prints this help screen.")
    print("   - '-s' and '--simple-precision' : Forces a simple precision on the solver.")
    print("   - '-r' and '--random-rhs' : Generates randomly the right-hand side, if it \nwasn't imported before.")

def _decode(string):
    if sys.version_info.major == 2:
        return string.decode('utf-8')
    else:
        return string

def parser_splice(filepath):
    """
    Basic splicing for parsing the config file.
    """
    conffile = open(filepath, 'r')
    content = _decode(conffile.read())
    lines = content.split('\n')

    strip_cw(lines)

    i = 0
    while i < len(lines):
        lines[i] = lines[i].split(' = ')
        i +=1

    return lines

def strip_cw(t):
    i = 0
    while i < len(t):
        if t[i] == '':
            t.remove(t[i])
            i -= 1
        elif t[i][0] == u'#':
            t.remove(t[i])
            i -= 1
        i += 1


def sparse_mat_import(filepath, is_precision_double):
    """
    Automatic importation of sparse matrix from an external file. Supported
    formats are Matrix Market .mtx format and Coordinate .ijv format.
    """
    matfile = open(filepath, 'r')

    name = matfile.name

    content = matfile.read()
    content = _decode(content)
    lines = content.split('\n')

    # File mode : 0 => IJV file ; 1 => Matrix Market file

    filemode = -1

########################################
# Precision setup
########################################

    if is_precision_double:
        datype = np.float64
    else:
        datype = np.float32

########################################
# File setup
########################################

    # .ijv files

    if name[-4:]=='.ijv':
        filemode = 1

    # .mtx/.mm files

    elif name[-4:]=='.mtx' or name[-3:]=='.mm':
        filemode = 2

########################################
# File treatment
########################################

    # IJV mode

    if filemode == 1:

        for i in range(len(lines)):
            lines[i] = lines[i].split(' ')
            strip_cw(lines[i])

        if len(lines[1]) == 3:
            is_complex = False
        elif len(lines[1]) == 4:
            is_complex = True
        else:
            raise ValueError("Matrix file not recognized")

        length = int(lines[0][0])
        width = int(lines[0][1])

        rows = np.zeros((int(lines[0][2])), dtype=np.int32)
        cols = np.zeros((int(lines[0][2])), dtype=np.int32)
        if not(is_complex):
            data = np.zeros((int(lines[0][2])), dtype=datype)
        else:
            data = np.zeros((int(lines[0][2])), dtype=complex)


        for i in range(len(rows)):
            rows[i] = int(lines[i+1][0]) - 1
            cols[i] = int(lines[i+1][1]) - 1
            if not(is_complex):
                data[i] = datype(lines[i+1][2])
            else:
                data[i] = datype(lines[i+1][2]) + datype(lines[i+1][3]) * 1j


    # MTX mode

    elif filemode == 2:
        if 'real' in lines[0]:
            is_complex = False
        elif 'complex' in lines[0]:
            is_complex = True

        for i in range(len(lines)):
            lines[i] = lines[i].split(' ')
            strip_cw(lines[i])

        length = int(lines[1][0])
        width = int(lines[1][1])
        n_data = int(lines[1][2])

        rows = np.zeros((n_data), dtype=np.int32)
        cols = np.zeros((n_data), dtype=np.int32)

        if not(is_complex):
            data = np.zeros((n_data), dtype=datype)
        else:
            data = np.zeros((n_data), dtype=complex)

        for i in range(len(rows)):
            rows[i] = int(lines[i+2][0]) - 1
            cols[i] = int(lines[i+2][1]) - 1
            if not(is_complex):
                data[i] = datype(lines[i+2][2])
            else:
                data[i] = datype(lines[i+2][2]) + datype(lines[i+2][3]) * 1j

    matfile.close()

    A = spp.coo_matrix((data, (rows, cols)), shape=(length, width), dtype = complex if is_complex else datype)

    return A, length, width, is_complex

def generate_rhs(matrix, length, width, NRHS, random_rhs, is_complex, is_precision_double):
    """
    Automatic generation of right-hand side if it isn't already supplied by the
    user in the config file.
    """

    if is_complex:
        scaltype= np.complex128 if is_precision_double else np.complex64
    else:
        scaltype= np.float64 if is_precision_double else np.float32

    if NRHS == 1:
        sol_shape = (width,)
    else:
        sol_shape = (width, NRHS)

    if random_rhs:

        th_sol = np.empty((sol_shape), dtype=scaltype)
        if is_complex:
            sol_gen = np.random.ranf(sol_shape) + np.random.ranf(sol_shape)*1.0j
        else:
            sol_gen = np.random.ranf(sol_shape)
        th_sol[:] = sol_gen[:]

    else:

        th_sol = np.arange(1, width*NRHS+1, dtype=scaltype).reshape(sol_shape)
        if is_complex:
            th_sol[:] += 1.0j

    rhs = np.matmul(matrix.todense(), th_sol)

    return rhs, th_sol
