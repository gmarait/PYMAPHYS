# -*- coding:utf-8 -*-

from mpi4py import MPI
from pymaphys import Maphys
from preproc import sparse_mat_import, parser_splice, generate_rhs, get_help
from sys import argv
import numpy as np

# MPI stuff
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
nprocs = comm.Get_size()

if len(argv) == 2 and (argv[1] == '--help' or argv[1] == '-h'):
    if rank == 0:
        get_help()

elif len(argv) >= 2:

    # Precision setup
    if '-s' in argv or '--simple-precision' in argv and rank == 0:
        is_precision_double = False
    else:
        is_precision_double = True

    # RHS generation setup
    if '-r' in argv or '--random-rhs' in argv and rank == 0:
        is_rhs_random = True
    else:
        is_rhs_random = False

    #Splicing of the config file
    tab = parser_splice(argv[-1])

    ######################################
    # Parsing of the config file
    ######################################

    matfile_found = False
    sym_info = False
    rhs_found = False
    is_complex = False

    i = 0

    while i < len(tab) and not(tab[i][:5] == u"INCTL"):
        if tab[i][0] == u"MATFILE":
            matrix_path = tab[i][1]
            matfile_found = True

        if tab[i][0] == u"SYM" :
            sym = int(tab[i][1])
            sym_info = True

        if tab[i][0] == u"RHSFILE":
            rhs_path = tab[i][1]
            rhs_found = True

        i += 1

    if not(matfile_found):
        raise ValueError("No Main Matrix found")

    if not(sym_info):
        raise ValueError("No symmetry information")

    if rank == 0:
        print("Importing matrix")
        A, width, length, is_complex = sparse_mat_import(matrix_path, is_precision_double)

    is_complex = comm.bcast(is_complex, root = 0)

    if rank == 0:
        if rhs_found:
            print("Importing right-hand side")
            rhs, rhs_w, NRHS, ic2 = sparse_mat_import(rhs_path, is_precision_double)
            rhs = rhs.todense()
        else:
            rhs, th_sol = generate_rhs(A, length, width, 1, is_rhs_random, is_complex, is_precision_double)

    # Initialize maphys wrapper
    if is_complex :
        if is_precision_double:
            driver_maphys = Maphys('z')
        else:
            driver_maphys = Maphys('c')
    else :
        if is_precision_double:
            driver_maphys = Maphys('d')
        else:
            driver_maphys = Maphys('s')

    for i in range(len(tab)):
        if tab[i][0][:5] == u"ICNTL":
            if tab[i][0][7] == u')':
                driver_maphys.ICNTL[int(tab[i][0][6])] = int(tab[i][1])
            else:
                driver_maphys.ICNTL[int(tab[i][0][6:8])] = int(tab[i][1])

        if tab[i][0][:5] == u"RCNTL":
            if tab[i][0][7] == u')':
                driver_maphys.RCNTL[int(tab[i][0][6])] = float(tab[i][1])
            else:
                driver_maphys.RCNTL[int(tab[i][0][6:8])] = float(tab[i][1])

    # Get corresponding numpy type
    nptype = driver_maphys.nptype

    # Set fortran communicator
    mphs = driver_maphys.mphs
    driver_maphys.set_comm(comm)
    mphs.sym = sym

    # Init maphys
    driver_maphys.initialize()


    if rank == 0:
        driver_maphys.set_A(A)
        driver_maphys.set_RHS(rhs)

    # Call maphys
    driver_maphys.drive(6)

    be = mphs.rinfog[1]

    if rank == 0:
        sol = driver_maphys.get_solution()
        print("Found:")
        print(sol[:min(20,len(sol))])
        if not(rhs_found):
            print("Th. solution:")
            print(th_sol[:min(20,len(th_sol))])
        print("Backward error:")
        print(be)

    # Finalize maphys
    driver_maphys.finalize()

    if rank == 0:
        print("done")

else:
    raise RuntimeError("Command does not know how to work. Please run with option '-h' or '--help'.")
